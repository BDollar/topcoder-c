#include <iostream>
#include <sstream>
#include <string>
#include <stdio.h>
using namespace std;

//This creates the class wrapper and names it 'Substitute'.
class Substitute
{
public:
    //This creates a method for the Substitute class and names it 'getValue' (Substitute::getValue).
    int getValue(string key, string code)
    {
        //This creates an integer container for the string stream to populate and return as the value of the item.
        int value = 0;
        
        //This sets up a loop that goes to each letter of the string for validation.
        for(int i=0;i<key.length()-1;i++)
        {
            // This removes non-letters and notifies the console user each time.
            if(not(isalpha(key[i])))
            {
                cout << "Removing " << key[i] << " since it is not a letter.\n";
                key.erase(i,1);
                i = i-1;
            }
            
            //If the character is a letter, it is converted to uppercase.
            else
            {
                // This converts the character to uppercase.
                key[i] = (toupper(key[i]));
                
                //This is a loop that ends at the index of the main loop.
                for(int x=0;x<i;x++)
                {
                    //This checks to see if the letter is a duplicate of an earlier letter in the string
                    if((key[i]==key[x])&&(i!=x))
                    {
                        //This announces the removal of the duplicate character
                        cout << "Removing extra " << key[i] << " since it is a duplicate.\n";
                        //This removes the duplicate character
                        key.erase(i,1);
                        //This compensates for the erasure's effect on the array
                        i=i-1;
                    }
                }
            }
        }
        
        //After all of the invalid characters are removed, if there are not enough characters, an error is returned
        if (key.length()<10)
        {
            cout << "The key entered does not have enough letters:" << key << "\n";
        }
        else
        {
            //Moves the 10th character to the front of the string so that it will represent 0 instead of 10 per the specs.
            key = key.substr(9,1)+key.substr(0,9);
            
            //Creates an output string stream named 'oss'.  This will help convert the string into a integer requested by the class specs.
            ostringstream oss;
            
            //This creates a loop that stops at the end of the code string
            for(int i=0;i<code.length();i++)
            {
                //if the code is found in the first 10 characters in the key the sequence number of that character in the key is added to the stream
                if (key.find(code[i])<=9)
                {
                    oss << key.find(code[i]);
                }
            }
            
            //Creates an input string stream from a string in the output string stream
            istringstream iss(oss.str());
            
            //Dumps iss into value
            iss >> value;
        }
        //This passes the integer value back to the method that called it.
        return value;
    }
};

//The next section was for testing the class.  I remarked it to submit the code.
/*
int main ()
{
    string key1, code1;
    Substitute Substitute1;

    cout << "Please enter a sequence of ten letters to represent the key: ";
    cin >> key1;
    cout << "Please enter the code to be translated into a value: ";
    cin >> code1;
    cout << Substitute1.getValue(key1,code1) << "\n";
    return 0;
 }
 */
