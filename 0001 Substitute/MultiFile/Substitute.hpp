//
//  Substitute.hpp
//  Substitute
//
//  Created by Brandon Dollar on 4/3/16.
//  Copyright © 2016 Brandon Dollar. All rights reserved.
//

#ifndef Substitute_hpp
#define Substitute_hpp
#include <string>

class Substitute
{
    private:
        std::string key_local;
        std::string code_local;
    
    public:
        Substitute(std::string key, std::string code);
        std::string Decode(std::string key_local, std::string code_local);
};

#endif /* Substitute_hpp */
