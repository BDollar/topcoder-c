//
//  Substitute.cpp
//  Substitute
//
//  Created by Brandon Dollar on 4/4/16.
//  Copyright © 2016 Brandon Dollar. All rights reserved.
//

#include "Substitute.hpp"
#include <iostream>
#include <sstream>
#include <iostream>
//#include <string>

using namespace std;

int Substitute::getValue(string key, string code)
{
    int valueInt = 0;  //Creates an integer container for the string stream to populate

    for(int i=0;i<key.length()-1;i++)
    {
        if(not(isalpha(key[i])))
        {
            cout << "Removing " << key[i] << " since it is not a letter.\n";
            key.erase(i,1);
            i = i-1;
        }
        else
        {
            char c = key[i];
            key[i] = (toupper(c)); // Converts lowercase to uppercase.
            for(int x=0;x<(i);x++)
            {
                if((key[i]==key[x])&&(i!=x))
                {
                    cout << "Removing extra " << key[i] << " since it is a duplicate.\n";
                    key.erase(i,1);
                    i=i-1;
                }
            }
        }
    }
    if (key.length()<10)
    {
        cout << "The key entered does not have enough letters:" << key << "\n";
    }
    else
    {
        key = key.substr(9,1)+key.substr(0,9); //Moves the 10th character to the front of the string
        ostringstream oss; //Creates an output string stream
    
        for(int i=0;i<code.length();i++)
        {
            if (key.find(code[i])<=9)
            {
                oss << key.find(code[i]);
            }
        }
    
        istringstream iss(oss.str()); //Creates an input string stream from a string in the output string stream
        iss >> valueInt; //Dumps iss into valueInt
    }
        return valueInt;

}