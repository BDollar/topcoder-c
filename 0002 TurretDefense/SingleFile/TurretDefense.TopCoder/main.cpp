//
//  main.cpp
//  TurretDefense.TopCoder
//
//  Created by Brandon Dollar on 5/2/16.
//  Copyright © 2016 Brandon Dollar. All rights reserved.
//
#include "TurretDefense.hpp"
#include <iostream>

int main()
{
    
    TurretDefense Test1;
    
    cout << Test1.firstMiss(    {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16}
                            ,{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16}
                            ,{2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,31}) << "\n";
    
    return 0;
}
