//
//  TurretDefense.cpp
//  TurretDefense.TopCoder
//
//  Created by Brandon Dollar on 5/2/16.
//  Copyright © 2016 Brandon Dollar. All rights reserved.
// 
 
 */
#include "TurretDefense.hpp"
#include <vector>
#include <iostream>
#include <complex>
using namespace std;

    int TurretDefense::firstMiss(vector <int> xs, vector <int> ys, vector <int> times)
    {
        int i, posx = 0, posy = 0, start_time = 0, travel_time = 0;
        
        for(i=0;i<times.size();i++) // Sets up a loop that stops at the end of the times list.
        {
            travel_time = abs(posx-xs[i])+abs(posy-ys[i]); // measures travel time
            
            if(start_time+travel_time>times[i]) //checks the time used vs the target arrival time
            {
                break;
            }
            else
            {
                if(i==times.size()-1) // If this is the last target, change i to the 100% success flag and stop the loop
                {
                    i=-1;
                    break;
                }
                else
                {
                    posx=xs[i];posy=ys[i]; // otherwise set the old position to the current position
                    start_time=times[i]; // and
                }
            }
        }

        return i;
    }



