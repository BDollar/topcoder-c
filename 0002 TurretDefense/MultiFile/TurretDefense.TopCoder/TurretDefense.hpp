//
//  TurretDefense.hpp
//  TurretDefense.TopCoder
//
//  Created by Brandon Dollar on 5/2/16.
//  Copyright © 2016 Brandon Dollar. All rights reserved.
//

#ifndef TurretDefense_hpp
#define TurretDefense_hpp
#include <vector>
#include <stdio.h>
using namespace std;

class TurretDefense
{
    public: int firstMiss(vector <int> xs, vector <int> ys, vector <int> times);
};

#endif /* TurretDefense_hpp */
