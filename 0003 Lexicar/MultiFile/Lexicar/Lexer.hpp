//
//  Lexer.hpp
//  Lexicar
//
//  Created by Brandon Dollar on 5/8/16.
//  Copyright © 2016 Brandon Dollar. All rights reserved.
//

#ifndef Lexer_hpp
#define Lexer_hpp
#include <stdio.h>
#include <vector>
#include <string>
using namespace std;

class Lexer
{
    public: vector <string> tokenize(vector <string> tokens, string input);
};

#endif /* Lexer_hpp */
