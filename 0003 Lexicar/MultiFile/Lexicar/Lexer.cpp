//
//  Lexer.cpp
//  Lexicar
//
//  Created by Brandon Dollar on 5/8/16.
//  Copyright © 2016 Brandon Dollar. All rights reserved.
//

#include "Lexer.hpp"
#include <iostream>
using namespace std;

vector <string> Lexer::tokenize(vector <string> tokens, string input)
{
    vector <string> consumed;
    
    if(tokens.size()>50)
    {
        tokens.resize(50);
    }
    
    //The next section sorts the tokens from largest to smallest
    
    int swap_count = 0; //this tracks whether the sort needs to happen again
    
    do
    {
        swap_count = 0; // set the swap count to zero
        
        for(int i=0; i<tokens.size(); i++) //loop that runs the length of the 'tokens' string
        {
            if(tokens[i].length()<tokens[i+1].length()) // if this token is smaller in length than the next token
            {
                tokens[i].swap(tokens[i+1]); //swap the tokens
                swap_count++; //add one to the swap count
            }
        }
    }
    while(swap_count!=0); //while there are swaps
    
    //The next section consumes the input string.
    while(input.length()>0)
    {
        int count_tokens_consumed=0;
        
        for(int i=0; i<tokens.size(); i++) // loop set up to go through the units in the tokens vector
        {
            if(tokens[i]==input.substr(0,tokens[i].length())) //if the current token matches the first part of the input
            {
                consumed.push_back(tokens[i]); //add the token to the consumed vector
                input = input.substr(tokens[i].length()); //remove the token from the front of the input string
                count_tokens_consumed++;
                i=int(tokens.size());
            }
        }
        
        if (count_tokens_consumed==0)
        {
            input = input.substr(1);//or remove the first character on no match
        }
    }
    
    return consumed;
}

