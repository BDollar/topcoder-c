//
//  WidgetRepairs.cpp
//  
//
//  Created by Brandon Dollar on 6/1/16.
//
//

#include <vector>
#include <numeric>
#include <iostream>
using namespace std;

class WidgetRepairs //class layout
{
    public: int days(vector<int> arrivals,int numPerDay);
};

int WidgetRepairs::days(vector<int> arrivals,int numPerDay) // class method used to activate the procedure
{
    int widgetInventory=0, //unrepaired widgets at the shop
        widgetsRepaired=0, //widgets repaired that day
        workdays=0, //tracks the number of total days spent repairing
        estimatedDaysLeft=0, //helps add days (without arrivals) to the end of the arrivals vector once the arrivals end in order to finish tallying the workdays
        widgetsRemaining = accumulate(arrivals.begin(), arrivals.end(),0); //countdown to zero unrepaired widgets
    
    for(int i=0;i<arrivals.size();i++) //cycles through the arrivals vector, representing days
    {
        widgetInventory = widgetInventory + arrivals[i]; //acts as a receiving department
        
        if(widgetInventory != 0) //if there are unrepaired widgets at the shop that day..
        {
            widgetsRepaired = min(numPerDay,widgetInventory); // ..repair unrepaired inventory widgets until the daily limit is reached..
            widgetInventory -= widgetsRepaired; // ..and remove them from the inventory of unrepaired widgets..
            widgetsRemaining -= widgetsRepaired; // ..and remove them from the global count of unrepaired widgets..
            workdays++; // ..and add a workday.
            
            if(i == arrivals.size()-1 && (widgetsRemaining)>0) // if this is the last day listed in the arrivals vector..
            {
                estimatedDaysLeft = widgetsRemaining / numPerDay; // ..find out the estimated days left..
                arrivals.push_back(estimatedDaysLeft); // ..and add it to the arrivals vector
            }
        }
    }
    return workdays;  // once the arrivals vecor is completed, return the number of worked days.
}

int main()
{
    WidgetRepairs WidgetRepairs1;
    cout << WidgetRepairs1.days({10,0,0,4,20},8) << endl;
    
}